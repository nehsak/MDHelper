from asyncio import subprocess
import os, shutil
from sqlalchemy import false
from win32api import RegCloseKey, RegOpenKey, RegQueryValueEx, RegSetValueEx
from win32con import HKEY_CURRENT_USER, KEY_READ, KEY_WRITE, REG_SZ
import subprocess
REGSTEAMROOT = 'SOFTWARE\\Valve\\Steam'

def getSteamInstallPath():
    try:
        hkey = RegOpenKey(HKEY_CURRENT_USER, REGSTEAMROOT, 0, KEY_READ)
        if hkey:
            v = RegQueryValueEx(hkey,'SteamPath')
            RegCloseKey(hkey)
            return v[0]
    except:
        pass


def getUserList():
    steampath = getSteamInstallPath()
    ret = []
    if steampath is None:
        print('未找到Steam')
        return
    steamLoginUserPath = steampath + '/config/loginusers.vdf'
    with open(steamLoginUserPath, 'r', encoding='utf-8') as f:
        line = f.readline()
        while (line):
            kv = line.split()
            if len(kv) == 2:
                if kv[0] == '"AccountName"':
                    ret.append([kv[1].strip('"')])
                if kv[0] == '"PersonaName"':
                    ret[-1].append(kv[1].strip('"'))
            line = f.readline()

    return ret

def loginUser(account):
    os.system('taskkill /f /im steam.exe')
    os.system('taskkill /f /im SteamService.exe')
    try:
        hkey = RegOpenKey(HKEY_CURRENT_USER, REGSTEAMROOT, 0, KEY_WRITE)
        if hkey:
            RegSetValueEx(hkey, 'AutoLoginUser', 0, REG_SZ, account[0])
            RegSetValueEx(hkey, 'LastGameNameUsed', 0, REG_SZ, account[1])
            RegCloseKey(hkey)
            steampath = getSteamInstallPath() + '/steam.exe'
            subprocess.Popen(steampath)
    except:
        pass

def renameMDDataFile(hash):
    localdatadir = getSteamInstallPath() + '/steamapps/common/Yu-Gi-Oh!  Master Duel/LocalData'
    src = localdatadir + '/'+ os.listdir(localdatadir)[0]
    des = localdatadir + '/'+ hash
    if src == des:
        return
    shutil.move(src, des)
