from ui_main import Ui_dialog

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys
import json
from steamlogin import loginUser, getUserList, renameMDDataFile

class DLGMDHelper(QDialog):

    def saveTable(self):
        rowCount = self.usertable.rowCount()
        columnCount = self.usertable.columnCount()
        userlst = []
        for i in range (rowCount):
            user = []
            for j in range(columnCount):
                user.append(self.usertable.item(i, j).text())
            userlst.append(user)

        try:
            with open('users.json','w') as f:
                f.write(json.dumps(userlst))
        except:
            pass

    def initUserTable(self):
        userlist = []

        try:
            with open('users.json','r') as f:
                userlist = json.loads(f.read())
        except:
            pass

        if not userlist:
            accountlist = getUserList()
            for account,name in accountlist:
                userlist.append([account, name, '',''])
        
        userCount = len(userlist)
        self.usertable.setRowCount(userCount)

        for i in range(userCount):
            for j in range(len(userlist[0])):
                self.usertable.setItem(i, j, QTableWidgetItem(userlist[i][j]))

    def login(self):
        currentrow = self.usertable.currentRow()
        account =  self.usertable.item(currentrow, 0).text()
        name =  self.usertable.item(currentrow, 1).text()
        hash =  self.usertable.item(currentrow, 2).text()
        loginUser([account,name])
        if len(hash) == 8:
            renameMDDataFile(hash)

    def closeEvent(self, arg__1) -> None:
        self.saveTable()
        return super().closeEvent(arg__1)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        Ui_dialog().setupUi(self)
        self.usertable:QTableWidget  = self.findChild(QTableWidget, 'tableWidget')
        self.btnlogin:QPushButton  = self.findChild(QPushButton, 'pushButton_login')
        self.btnlogin.clicked.connect(self.login)
        self.initUserTable()

if __name__ == "__main__":
    app = QApplication([])
    window = DLGMDHelper()
    if not window:
        sys.exit()
    window.show()
    sys.exit(app.exec_())